# -*- coding: utf-8 -*-
"""
copyright (c) 2018
@author: Katariina Latvala
@license: <https://gitlab.com/klel/LBlastPycli/blob/master/LICENSE.txt>
"""
import setuptools


with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    entry_points = {
            'console_scripts': [
                    'LBlastPycli=LBlastPycli.LBlastPycli.__main__:main',
                    ],
                    },
    name="LBlastPycli",
    version="1.0",
    author="Katariina Latvala",
    author_email="kata.latvala@gmail.com",
    description="A python command line tool that uses NCBI's blast " + \
    "on local nucleotide databases.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/klel/LBlastPycli",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3.6",
        "License :: OSI Approved :: BSD 3-Clause License",
        "Operating System :: OS Independent",
    ),
)
