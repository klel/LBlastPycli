# Local Blast Python Command Line Interface

A simple command line tool built with Python and [NCBI's BLAST+ command-line tools](https://blast.ncbi.nlm.nih.gov/Blast.cgi?CMD=Web&PAGE_TYPE=BlastDocs&DOC_TYPE=Download), that allows the user to run blast (blastn or tblastn) against their local nucleotide sequence databases, and create FASTA files from the top hits between each query sequence and each database. The user provides two mandatory input files: a queries FASTA file, and a .txt file listing paths to each blast database to be used. The user can also provide three optional input values to: change the e-value cut off point, limit how many of the top hits are included in the results files, and whether the hits are in order (by evalue) in the results files.

The tool is meant to be an exploratory tool, where the user can find the sequences in their local data that match the queries, and then have the results in ready FASTA files to feed back to NCBI's web-based Blast to search them against all the global databases and to get detailed information of the alignments.

## Getting Started

The tool is available as a Python application (lblastpycli_py.zip) for developers as well as an executable program (lblastpycli_winx64_exe.zip) for end users. The executable was created for a Windows 64x OS with [PyInstaller](http://www.pyinstaller.org/), but executables for other environments can be easily created from the original script files available on GitLab.

### Prerequisites

For running Python application:
* Python 3.x
* Biopython 1.71
* NCBI Blast+ 2.7.1

For running executable:
* NCBI Blast+ 2.7.1

### Built With

* Python 3.6 [PSF License](https://docs.python.org/3.6/license.html)
* Biopython 1.71 [Biopython License Agreement and BSD 3-Clause License](https://github.com/biopython/biopython/blob/master/LICENSE.rst)
* NCBI Blast+ 2.7.1 [Public Domain](https://www.ncbi.nlm.nih.gov/IEB/ToolBox/CPP_DOC/lxr/source/scripts/projects/blast/LICENSE)
* Pyinstaller 3.3.1 [License GPL (with special exception)](https://raw.githubusercontent.com/pyinstaller/pyinstaller/develop/COPYING.txt)

## Authors

Katariina Latvala kata.latvala@gmail.com

## License
 
See LICENSE.txt file

