# -*- coding: utf-8 -*-
"""
copyright (c) 2018
@author: Katariina Latvala
@license: <https://gitlab.com/klel/LBlastPycli/blob/master/LICENSE.txt>
"""
from Bio.Blast import NCBIXML
import os
import time
from elapsed_time_thread import ElapsedTimeThread
from funcmodule import parse_arguments, blast, get_sorted_hits_names,\
create_result_file

def main():

    thread = ElapsedTimeThread()
    start = time.time()
    
    try:
        (query_file, dbs_file, blast_type, evalue, limit, ordered) = \
        parse_arguments()
        
        thread.start()
          
        with open(dbs_file, "r") as db_handle:
        
            for line in db_handle:
                db = line.strip()
                
                blast_out = blast(query_file, db, blast_type, evalue)
                
                with open(blast_out) as results_handle:
                    blast_records = NCBIXML.parse(results_handle)
                    
                    for record in blast_records:
                        print("                                          " + \
                              "                                           \r")
                        print("\r > " + record.query_id + ": " + record.query)
                        nro_hits = 0
                        
                        if record.alignments:
                            (nro_hits, hit_names) = \
                            get_sorted_hits_names(record)     
                            top_N = min(nro_hits, limit)
                            
                        print("Total number of hits: " + str(nro_hits))
                            
                        if nro_hits > 0:
                            print("creating file for top " + str(top_N) + \
                              " hits")                
                            hit_names = hit_names[0:top_N]
                            
                            query_name = os.path.splitext(query_file)[0]
                            db_name = os.path.splitext(os.path.basename
                               (db))[0]
                            result_file_name = query_name + "_" + \
                            str(record.query_id) + "_top_" + str(top_N) + \
                            "_" + db_name + "_results.txt"
                            
                            create_result_file(result_file_name, hit_names, \
                             db, top_N, ordered)
            # elapsed time overwiriting this bit with faster computer, fix!
                print("-----")
        
        thread.stop()
        thread.join()
        print("Finished in {:.1f} seconds".format(time.time()-start))
    
    except KeyboardInterrupt:
        thread.stop()
        thread.join()
        print("Program terminated by user at {:.1f} seconds."
          .format(thread._elapsed_time))
    
    except Exception as e: 
        thread.stop()
        thread.join()
        print("Program interrupted by an internal error at {:.1f} seconds.\n"
          .format(thread._elapsed_time))
        print(e)
    
if __name__ == '__main__':
    main()