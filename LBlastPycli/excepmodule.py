# -*- coding: utf-8 -*-
"""
copyright (c) 2018
@author: Katariina Latvala
@license: <https://gitlab.com/klel/LBlastPycli/blob/master/LICENSE.txt>
"""

class Error(Exception):
    pass

class ValueError(Error):
    
    def __init__(self, expression, message):
        self.expression = expression
        self.message = message

class FileFormatError(Error):
    
    def __init__(self, expression, message):
        self.expression = expression
        self.message = message

 