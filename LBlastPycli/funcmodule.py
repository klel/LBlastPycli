# -*- coding: utf-8 -*-
"""
copyright (c) 2018
@author: Katariina Latvala
@license: <https://gitlab.com/klel/LBlastPycli/blob/master/LICENSE.txt>
"""
import argparse
import numpy as np
import os
from Bio import SeqIO
from Bio.Blast.Applications import NcbiblastnCommandline, NcbitblastnCommandline

from excepmodule import FileFormatError


def parse_arguments():
    parser = argparse.ArgumentParser(description="A python cli tool that" + \
                                     " runs blast between specified queries" +\
                                     " and local nucleotide blast databases,"+\
                                     " and creates FASTA files of top hits.")
    parser.add_argument("query_file", help="FASTA file with query sequences")
    parser.add_argument("dbs_file", help="text file of paths to " + \
                        "database files")
    parser.add_argument("blast_type", help="use 'tblastn' for protein vs. " + \
                        "translated nuocleotide blast or 'blastn' for " + \
                        "nucleotide vs. nucleotide blast")
    parser.add_argument("-evalue", help="evalue cutoff in range (0,1), " + \
                        "default = 0.001", default=0.001, 
                        type=check_probability)
    parser.add_argument("-limit", help="integer limit for the number of " + \
                        "hits to include in each result file, default = " + \
                        "unlimited", type=check_positive)
    parser.add_argument("-ordered", help="switch to have hits in " + \
                        "order of evalue in result files", action="store_true")
    args = parser.parse_args()
    
    validate_arguments(args)
    
#    if args.evalue:
#        evalue = args.evalue
#    else:
#        evalue = 0.001      
            
    if args.limit:
        limit = int(args.limit)
    else:
        limit = np.inf
    
    if args.ordered:
        ordered = True
    else:
        ordered = False
    
    return (args.query_file, args.dbs_file, args.blast_type, args.evalue, limit,\
            ordered)

    
def validate_arguments(args):  
    try:
        is_fasta(args.query_file)
    except FileFormatError as err:
        print("FileformatError: {} {}.".format(err.expression, err.message))
        quit()

def check_probability(x):
    x = float(x)
    if x <= 0 or x >= 1:
        raise argparse.ArgumentTypeError("{} not in range (0,1)".format(x))
    return x

def check_positive(value):
    ivalue = int(value)
    if ivalue <= 0:
        raise argparse.ArgumentError("{} is an invalid positive integer."
                                     .format(ivalue))
    return ivalue
        
# Biopython doesn't seem to have any exception handling when parsing files
# that are not FASTA formatted, but are inputted as such.
# N.B. does not check whether contents are valid, only for FASTA formatting.
def is_fasta(file_path):
    with open(file_path, "r") as handle:
        fasta = SeqIO.parse(handle, "fasta")
        if  not any(fasta):
            message = "file not in FASTA format"
            filename = os.path.splitext(os.path.basename(file_path))[0]
            raise FileFormatError(filename, message)
        else:
            return True


def blast(query, db, blast_type, evalue):
    query_name = os.path.splitext(os.path.basename(query))[0]
    db_name = os.path.splitext(os.path.basename(db))[0]
    print("\rRunning " + blast_type + " against the " + db_name + \
          " nucleotide database")
    
    blast_out = (query_name + "_" + "_" + db_name + "_output.xml")
    
    if blast_type == "tblastn":
        cline = NcbitblastnCommandline(query=query, db=db, evalue=evalue,
                                        outfmt=5, out=blast_out)
    else:
        cline = NcbiblastnCommandline(query=query, db=db, evalue=evalue,
                                        outfmt=5, out=blast_out)
    
    stdout, stderr = cline()
   
    return blast_out
    
    
def get_sorted_hits_names(record):
    nro_records = 0
    hit_names = []
    
    if record.alignments:
        for alignment in record.alignments:
                    hit_names.append(alignment.hit_def)
                    nro_records += 1
                    
    return (nro_records, hit_names)


def create_result_file(file_name, hit_names, database, top_N, ordered):
    if ordered:
        with open(file_name, "w+") as file:
            for name in hit_names:
                db_sequences = SeqIO.parse(open(database), 'fasta')
                for seq in db_sequences:
                    if seq.description == name:
                        SeqIO.write([seq], file, 'fasta')
    else:
        db_sequences = SeqIO.parse(open(database), 'fasta')
        with open(file_name, "w+") as f:
            for seq in db_sequences:
                if seq.description in hit_names:
                    SeqIO.write([seq], f, 'fasta')
    