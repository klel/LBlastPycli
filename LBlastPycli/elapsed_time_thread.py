# -*- coding: utf-8 -*-
"""
Created on Sun Jul 29 18:16:46 2018

@author: katal
"""
import threading
import time

class ElapsedTimeThread(threading.Thread):
    def __init__(self):
        super(ElapsedTimeThread, self).__init__()
        self._stop_event = threading.Event()
        self._elapsed_time = 0.0
    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()

    def run(self):
        thread_start = time.time()
        while not self.stopped():
            self._elapsed_time = time.time()-thread_start
            if self._elapsed_time > 1:
                print("Elapsed Time {:.1f} seconds\r"
                      .format(self._elapsed_time), end="")
                time.sleep(1)